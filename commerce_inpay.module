<?php

/**
 * @file
 * Module for the customer payment tests.
 */
define('INPAY_URL', 'https://secure.inpay.com');
define('INPAY_URL_TEST', 'https://test-secure.inpay.com');
define('NOTIFY_URL', 'inpay/notify');
define('PENDING_URL', 'inpay/pending');
define('CANCEL_URL', 'inpay/cancel');
define('ORDER_TEXT', 'Your order');
define('FLOW_LAYOUT', 'multi_page');

/**
 * Function to calculate checksum
 * Important to put the elements of $params array in the right order
 */
function generate_inpay_checksum($params) {
  $data = array();
  foreach ($params as $key => $value) {
    $data[$key] = $value;
  }
  $query_string = http_build_query($data, NULL, '&');
  return md5($query_string);
}

/**
 * Additional return, and notify, -urls 
 */
function commerce_inpay_menu() {
  $items[NOTIFY_URL] = array(
    'title' => 'Order complete',
    'page callback' => 'commerce_inpay_notify',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK
  );

  $items[PENDING_URL] = array(
    'title' => 'Order pending',
    'page callback' => 'commerce_inpay_pending',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK
  );

  $items[CANCEL_URL] = array(
    'title' => 'Order cancelled',
    'page callback' => 'commerce_inpay_cancel',
    'access arguments' => array('access content'),
    'type' => MENU_CALLBACK
  );
  return $items;
}

/**
 * page for for cancel/back
 */
function commerce_inpay_cancel()  {
	return ('<p>Your order has been cancelled.</p><a href="/">Return to frontpage.</a>');
}
 

/**
 * page for for pending
 */
function commerce_inpay_pending()  {
	$content = '<p>Your order is currently pending, and you will be notified as soon as the transfer has completed.</p>';
	return ($content);
}
 
/**
 * Callback for notifications 
 */
function commerce_inpay_notify() {
  //get the payment methods settings
  $payment_method = commerce_payment_method_instance_load('commerce_inpay|commerce_payment_commerce_inpay');

  //collect data for generating checksum
  if($_POST['version']=='2.0.0')  {
	  $params = array(
	    'api_version' => $_POST['api_version'],
	    'invoice_amount' => $_POST['invoice_amount'],
	    'invoice_currency' => $_POST['invoice_currency'],
	    'invoice_reference' => $_POST['invoice_reference'],
	    'invoice_status' => $_POST['invoice_status'],
	    'invoice_updated_at' => $_POST['invoice_updated_at'],
	    'merchant_id' => $_POST['merchant_id'],
	    'order_id' => $_POST['order_id'],
	    'received_sum' => $_POST['received_sum'],
	    'secret_key' => $payment_method['settings']['inpay_secret'],
	    'settlement_amount' => $_POST['settlement_amount'],
	    'settlement_currency' => $_POST['settlement_currency']
	  );
  }
  else {
  		
//[:order_id, :invoice_reference, :invoice_amount, :invoice_currency, :invoice_created_at, :invoice_status, :secret_key]
	  $params = array(
	    'order_id' => $_POST['order_id'],
	    'invoice_reference' => $_POST['invoice_reference'],
	    'invoice_amount' => $_POST['invoice_amount'],
	    'invoice_currency' => $_POST['invoice_currency'],
	    'invoice_created_at' => $_POST['invoice_created_at'],
	    'invoice_status' => $_POST['invoice_status'],
	    'secret_key' => $payment_method['settings']['inpay_secret'],
	  );
  }
  $status = $_POST["invoice_status"];

watchdog('POSTBACK:', print_r($_POST, TRUE));
watchdog('PARAMS:', print_r($params, TRUE));
watchdog('CHECKSUM:', generate_inpay_checksum($params));
watchdog('STATUS:', $status);

  if (generate_inpay_checksum($params) == $_POST['checksum'])  {
    $order_id = $_POST["order_id"];
	//load order
	watchdog('LOAD ORDER:', $order_id);
	$order = commerce_order_load($order_id);
	
	if($status == 'approved')  {
		watchdog('O STATUS:', 'complete order');
		
		$order = commerce_order_status_update($order, 'checkout_complete');
		commerce_checkout_complete($order);
		//commerce_order_status_update($order, 'completed');  //works on bestsellers
		//hook_commerce_payment_order_paid_in_full($order);
		//commerce_checkout_complete($order);
		//commerce_order_status_update($order, 'completed');  //works on bestsellers
		
		
		
		
				
		watchdog('O STATUS 2:','done');
	}
	else if($status == 'pending')  {
		watchdog('O STATUS:', 'pending order');
		commerce_order_status_update($order, 'pending');
	}
	else if($status == 'cancelled')  {
		watchdog('O STATUS:', 'cancelled');
		commerce_order_status_update($order, 'canceled');
	}
	else if($status == 'sum_too_low')  {
		watchdog('O STATUS:', 'pending order');
		commerce_order_status_update($order, 'pending_processing');
	}
	else if($status == 'refunded')  {
		watchdog('O STATUS:', 'refund order');
		commerce_order_status_update($order, 'checkout_canceled');
	}
	else if($status == 'error')  {
		watchdog('O STATUS:', 'error order');
		commerce_order_status_update($order, 'checkout_canceled');
	}
	
    watchdog('OK','ok');
    echo 'ok';
  }
  else  {
    watchdog('BAD','bad checksum');
	echo 'bad checksum';
  }
}


/**
 * Payment method callback: settings form.
 */
function commerce_inpay_settings_form($settings = NULL) {
  $form = array();
 
  // this create a configuration for a username to connect Paypal API
  $form['inpay_mid'] = @array(
    '#type' => 'textfield',
    '#title' => t('inpay Merchant ID'),
    '#description' => t('The Merchant ID, provided by inpay'),
    '#default_value' => $settings['inpay_mid'],
    '#required' => TRUE,
  );

  $form['inpay_secret'] = @array(
    '#type' => 'textfield',
    '#title' => t('inpay Secret Code'),
    '#description' => t('The secret code, provided by inpay'),
    '#default_value' => $settings['inpay_secret'],
    '#required' => TRUE,
  );
  $form['inpay_url'] = @array(
    '#type' => 'select',
    '#title' => t('Mode'),
    '#description' => check_plain(t('Select test, if you whish to test against the inpay test enviroment.<br>Note that you will need a separate MID/secret for the test enviroment.<br>Also remember to set up notify url to ' . $_SERVER['HTTP_HOST'] . '/inpay/notify in the inpay merchant interface at www.inpay.com')),
    '#default_value' => $settings['inpay_url'],
    '#required' => TRUE,
    '#options' => array(
      INPAY_URL_TEST => 'Test',
      INPAY_URL => 'Live'
    ),
    '#default_value' => INPAY_URL_TEST
  );
  return $form;
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function commerce_inpay_commerce_payment_method_info() {
  $payment_methods = array();
  $payment_methods['commerce_inpay'] = array(
    'title' => t('Bank transfer via inpay'),
    'short_title' => t('inpay'),
    'description' => t('inpay express payment'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'active' => TRUE,
  );
  return $payment_methods;
}

//function commerce_inpay_order_form($form, &$form_state, $order, $settings) {
//	watchdog('payment meth',print_r($settings,TRUE));
//}

/**
 * Payment method callback: redirect form.
 */
function commerce_inpay_redirect_form($form, &$form_state, $order, $payment_method) {
  global $base_url;
  
  
  
  // Return an error if the enabling action's settings haven't been configured.
  if (empty($payment_method['settings']['inpay_mid']) || empty($payment_method['settings']['inpay_secret']) ) {
    drupal_set_message(t('inpay integration i not proberly set up.'), 'error');
    return array();
  }

  $wrapper = entity_metadata_wrapper('commerce_order', $order);
  $currency_code = $wrapper->commerce_order_total->currency_code->value();
  $line_items = $wrapper->commerce_line_items;
  $total = $wrapper->commerce_order_total->amount->value();

  $buyer_address_info = $wrapper->commerce_customer_billing->commerce_customer_address->value();
  $buyer_info = 'INFO';

  $order_data = array();
  $order_data['merchant_id'] = $payment_method['settings']['inpay_mid'];
  $order_data['order_id'] = $order->order_id;
  $order_data['amount'] = ($total/100);
  $order_data['currency'] = $currency_code;
  $order_data['order_text'] = ORDER_TEXT;
  $order_data['flow_layout'] = FLOW_LAYOUT;
  $order_data['secret_key'] = $payment_method['settings']['inpay_secret'];

  $buyer_name = $buyer_address_info['name_line'];
  $buyer_address = '';

  if (isset($buyer_address_info['thoroughfare']))
    $buyer_address .=$buyer_address_info['thoroughfare'] . ' ';

  if (isset($buyer_address_info['premise']))
    $buyer_address .=$buyer_address_info['premise'] . ' ';

  if (isset($buyer_address_info['locality']))
    $buyer_address .=$buyer_address_info['locality'] . ' ';

  if (isset($buyer_address_info['postal_code']))
    $buyer_address .=$buyer_address_info['postal_code'] . ' ';

  //switch to textfield for debugging
  $type = 'textfield';
  //$type = 'hidden';

  $form['merchant_id'] = array(
    '#type' => $type,
    '#title' => t('MID'),
    '#value' => $payment_method['settings']['inpay_mid'],
    '#required' => TRUE,
  );
  $form['order_id'] = array(
    '#type' => $type,
    '#title' => t('OrderID'),
    '#value' => $order->order_id,
    '#required' => TRUE,
  );
  $form['amount'] = array(
    '#type' => $type,
    '#title' => t('Amount'),
    '#value' => ($total/100),
    '#required' => TRUE,
  );
  $form['currency'] = array(
    '#type' => $type,
    '#title' => t('Currency'),
    '#value' => $currency_code,
    '#required' => TRUE,
  );
  $form['order_text'] = array(
    '#type' => $type,
    '#title' => t('Order text'),
    '#value' => $order_data['order_text'],
    '#required' => TRUE,
  );
  $form['flow_layout'] = array(
    '#type' => $type,
    '#title' => t('Flow'),
    '#value' => $order_data['flow_layout'],
    '#required' => TRUE,
  );
  $form['buyer_email'] = array(
    '#type' => $type,
    '#title' => t('buyer_email'),
    '#value' => $order->mail,
    '#required' => TRUE,
  );
  $form['checksum'] = array(
    '#type' => $type,
    '#title' => t('checksum'),
    '#value' => generate_inpay_checksum($order_data),
    '#required' => TRUE,
  );
  $form['country'] = array(
    '#type' => $type,
    '#title' => t('country'),
    '#value' => $buyer_address_info['country'],
    '#required' => TRUE,
  );
  $form['buyer_name'] = array(
    '#type' => $type,
    '#title' => t('buyer name'),
    '#value' => $buyer_name,
    '#required' => TRUE,
  );
  $form['buyer_address'] = array(
    '#type' => $type,
    '#title' => t('buyer address'),
    '#value' => ($buyer_address),
    '#required' => TRUE,
  );
  $form['return_url'] = array(
    '#type' => $type,
    '#title' => t('Return URL'),
    '#value' => url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
    '#required' => TRUE,
  );
  $form['pending_url'] = array(
    '#type' => $type,
    '#title' => t('pending url'),
    '#value' => url('inpay/pending/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
    '#required' => TRUE,
  );
  $form['notify_url'] = array(
    '#type' => $type,
    '#title' => t('notify url'),
    '#value' => $base_url . '/' . NOTIFY_URL,
    '#required' => TRUE,
  );
  $form['cancel_url'] = array(
    '#type' => $type,
    '#title' => t('cancel url'),
    '#value' => url('inpay/cancel', array('absolute' => TRUE)),
    '#required' => TRUE,
  );
  $form['#action'] = $payment_method['settings']['inpay_url'];
  
  $img = drupal_get_path('module', 'commerce_inpay').'/img/inpay.png';
  
  $form['splash'] = array(
    '#type' => 'item',
    '#markup' => '<img src="/'.$img.'"><p><font size="-2">Bank tranfser via inpay</font></p>'
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed to inpay and complete payment'),
  );
  return $form;
}
